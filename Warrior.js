class Warrior {
  weapon;

  constructor(name, power, life) {
    this.name = name;
    this.power = power;
    this.life = life;
  }

  attack(opponent) {
    opponent.life -= this.power;
    return opponent.life;
  }

  isAlive() {
    return this.life > 0;
  }
}

class Weapon {
  constructor(name) {
    this.name = name;
  }
}

class WarriorAxe extends Warrior {
  constructor(name, power, life, weapon) {
    super(name, power, life);
    this.weapon = new Weapon("Axe");
  }
  attack(opponent) {
    if (opponent instanceof WarriorSword) {
      this.power *= 2;
    }
    return super.attack(opponent);
  }

  isAlive(){
    return super.isAlive();
  }
}

class WarriorSword extends Warrior {
  constructor(name, power, life, weapon) {
    super(name, power, life);
    this.weapon = new Weapon("Sword");
  }
  attack(opponent) {
    if (opponent instanceof WarriorSpear) {
      this.power *= 2;
    }
    return super.attack(opponent);
  }
  isAlive(){
    return super.isAlive();
  }
}

class WarriorSpear extends Warrior {
  constructor(name, power, life, weapon) {
    super(name, power, life);
    this.weapon = new Weapon("Spear");
  }
  attack(opponent) {
    if (opponent instanceof WarriorAxe) {
      this.power *= 2;
    }
    return super.attack(opponent);
  }
  isAlive(){
    return super.isAlive();
  }
}

export { Warrior, WarriorAxe, WarriorSpear, WarriorSword };
