import {Warrior, WarriorAxe, WarriorSpear, WarriorSword} from "./Warrior.js";

const joan = new Warrior("Joan", 5, 20);
const leon = new Warrior("Leon", 2, 10);
const ichida = new WarriorAxe("Ichida", 8, 25);
const ichigo = new WarriorSword("Ichigo", 10, 30);
const ikkaku = new WarriorSpear("Ikaku" , 6, 20);

console.log("Premier combat!");
console.log(`${joan.name} : ${joan.life}PV - ${joan.power} de puissance VS ${leon.name} : ${leon.life}PV - ${leon.power} de puissance`);
console.log(`${joan.name} attaque ${leon.name}. \nLes PV de ${leon.name} baissent à : ${joan.attack(leon)}`);
console.log(`${leon.name} est toujours en vie : ${leon.isAlive()}`);
console.log('\n');

console.log("Deuxième combat!");
console.log(`${ichida.name} : ${ichida.life}PV - ${ichida.power} de puissance VS ${ichigo.name} : ${ichigo.life}PV - ${ichigo.power} de puissance`);
console.log(`${ichida.name} attaque ${ichigo.name}. \nLes PV de ${ichigo.name} baissent à : ${ichida.attack(ichigo)}`);
console.log(`${ichigo.name} est toujours en vie : ${ichigo.isAlive()}`);
console.log('\n');

console.log("Troisième combat!");
console.log(`${ikkaku.name} : ${ikkaku.life}PV - ${ikkaku.power} de puissance VS ${ichida.name} : ${ichida.life}PV - ${ichida.power} de puissance`);
console.log(`${ikkaku.name} attaque ${ichida.name}. \nLes PV de ${ichida.name} baissent à : ${ikkaku.attack(ichida)}`);
console.log(`${ichida.name} est toujours en vie : ${ichida.isAlive()}`);
console.log('\n');

console.log("Dernier combat!");
const battle = (warriorOne, warriorTwo) => {
    while(warriorOne.life > 0 && warriorTwo.life > 0){
        warriorOne.attack(warriorTwo);
        warriorTwo.attack(warriorOne);
    }

    if(warriorOne.life <= 0 && warriorTwo.life <= 0){
        return "It's a draw";
    } else if(warriorOne.life > 0){
        return `${warriorOne.name} wins`
    } else{
        return `${warriorTwo.name} wins`
    }
}
const resultBattle = battle(ichigo, ikkaku);
console.log(resultBattle);